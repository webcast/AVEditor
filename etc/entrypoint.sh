#!/bin/bash

echo 'Starting app...'
# We run the application using uwsgi
uwsgi --ini /opt/app-root/src/server/uwsgi.ini
