## Webcast: AVEditor 
[https://aveditor.web.cern.ch/](https://aveditor.web.cern.ch/)

An automated video editing and encoding tool accessed though a web interface used for:

* Creating Picture in Picture videos
* Adding intro / outro video
* Encoding the video to a standard format \(H.264 for video stream, AAC for audio stream, original resolution mantained\)

## Requirements and dependencies

---

* Docker
* PostgreSQL DB
* Remote FFmpeg machine

## Installing and running on localhost

---

After cloning the repository: [https://gitlab.cern.ch/webcast/AVEditor.git](https://gitlab.cern.ch/webcast/AVEditor.git) and following the comment instructions in settings.py, run in the folder that Dockerfile resides in:

```
docker build -t aveditor .
```

```
docker run -p 8080:59999 aveditor
```

The container will run on **localhost:8080**

If you want to use OAuth during development on your own computer, you must register a new client on: https://sso-management.web.cern.ch/OAuth/RegisterOAuthClient.aspx


## Configuration

---
The following configuration settings can be changed in settings.py file

#### OAUTH

`CERN_OAUTH_CLIENT_ID`, `CERN_OAUTH_CLIENT_SECRET`, `CERN_OAUTH_AUTHORIZE_URL`, `CERN_OAUTH_TOKEN_URL`

#### MAILER

`MAIL_HOSTNAME`, `MAIL_FROM`

The following URL paths are used in the confirmation email

The Windows' path of the final video file

`MAIL_CONFIRM_WIN_PATH`

Another path of the final video file

`MAIL_CONFIRM_OTHER_PATH`

The path for the final location of the video file. However it is available only for Academic Training Videos.

`MAIL_CONFIRM_FINAL_LOCATION`

#### Intro and Outro Websites

The URL links for the intro and outro that you choose in the website

`E_LEARNING_INTRO`, `E_LEARNING_OUTRO`, `ACADEMIC_TRAINING_INTRO`, `ACADEMIC_TRAINING_OUTRO`

#### FFmpeg

Binaries to ffmpeg without trailing slash, Example: _/usr/bin/ffmpeg_

`FFMPEG_BIN`

Remote machine's path to a folder for intermediate processing. 

`FFMPEG_FILE_FOLDER`

#### SSH

SSH\_KEY_ will contain path like_: /path/to/ssh/key/id\_rsa

`SSH_SERVER`, `SSH_USERNAME`, `SSH_KEY`

#### Output Videos

The URL domain that it is supposed to have every video of the Academic Training

`URL_DOMAIN_INPUT_VIDEOS`

Remote machine's path for the output of the Academic Training videos. This is not the full path, just the start.

`PIP_FOLDER_PATH`

Remote machine's path for the output of every processed video.

`OUTPUT_FOLDER_PATH`

The filename of every new video of Academic Training

`PIP_OUTPUT_FILENAME`

#### Database

`DB_NAME`, `DB_PASS`, `DB_PORT`, `DB_SERVICE`, `DB_USER`

## FFmpeg commands

---

Some ffmpeg commands used by AVEditor. These commands are generate in the **app.utils.ffmpeg** Class


**Command used to cut a video given sart ad stop time (command-1)**

```
ffmpeg -i <path_to_video_file.mp4> -ss <start_time> -to <stop_time> <path_to_output_file.mp4>
```

start and stop time need to be in the following format: HH:MM:SS, for example:

```
ffmpeg -i <path_to_video_file.mp4> -ss 00:00:20 -t 00:02:45 <path_to_output_file.mp4>
```

**Command used to concatenate intro and outro to a video (command-2)**

```
ffmpeg -i <path_to_intro.mp4> -i <path_to_video_file.mp4> -i <path_to_outro.mp4> 
-filter_complex "
[0:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0];
[1:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v1]; 
[2:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v2];
[v0][0:a][v1][1:a][v2][2:a] concat=n=3:v=1:a=1[v][a]" 
-map "[v]" -map "[a]" final_video.mp4
```

**Complex filter**

For each slide we select the video stream `[<no_slide>:v]` and apply some filters like resizing and padding

Then we assign an alias to this video stream for the first slide `[v0]`, for second `[v1]` etc...
This is an important step in order to be able to concat two videos.

for example - `[0:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0];`

`[v0][0:a][v1][1:a][v2][2:a]` - Choose the order of the video streams, in this case we use the alias of the first generated video, concat the null audio src and the original video and audio stream of the input video

`concat=n=3:v=1:a=1` - Concatenate 3 video streams into 1 video stream and 1 audio stream


**Command used to create a PiP video and concatenate it with intro and outro (command-3)**

`intro.mp4` - the intro video
`camera_presentation_part.mp4` - the part of the camera video with the speaker's presentation. It is shown full screen. No slides are shown at this time.
`slides_main_part.mp4` - part of PiP. Slides will be shown at full screen
`camera_main_part.mp4` - part of PiP. Camera video will be shown in a small box at the right bottom corner. 
`camera_questions_part.mp4` - the part of the camera video with the questions. It is shown full screen. No slides are shown at this time.
`outro.mp4` - the outro video
```
ffmpeg -y -i intro.mp4 
-i camera_presentation_part.mp4 
-i slides_main_part.mp4 
-i camera_main_part.mp4 
-i camera_questions_part.mp4 
-i outro.mp4 
-filter_complex " 
[0:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0]; 
[1:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v1]; 
[2:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9,fps=fps=25[v2];
[3:v] scale=iw/3:ih/3 [v3]; 
[4:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v4]; 
[5:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v5]; 
[v2][v3] overlay=main_w-overlay_w-10:main_h-overlay_h-10[outv]; 
[v0][0:a][v1][1:a][outv][3:a][v4][4:a][v5][5:a] concat=n=5:v=1:a=1[v][a]" 
-map "[v]" -map "[a]" final_video.mp4 
```

This command creates the continuous Picture in Picture 

`[v2][v3] overlay=main_w-overlay_w-10:main_h-overlay_h-10[outv];`

This command creates the periodical Picture in Picture 

`[v2][v3] overlay=main_w-overlay_w-10:main_h-overlay_h-10:enable='lt(mod(t,120),20)[outv]; `

## Command sent via SSH

The command that the application sends to the remote machine
```
cd config['FFMPEG_FILE_FOLDER'] && 
directory=$(date +%s) && 
mkdir $directory && 
cd $directory &&  
mkdir -p config['OUTPUT_FOLDER_PATH']/indico_id && 
ffmpeg_commands &&
echo -e "body_message" | mailx -s smtp=config['MAIL_HOSTNAME'] -r config['MAIL_FROM'] -s "subject" -v username@cern.ch && 
cd .. && 
rm -r $directory -f 
```
The steps are 
- Go to the process folder **_config['FFMPEG_FILE_FOLDER']_**
- Create a new folder with name the current timestamp. (let's say **_folderA_** for this example)
- Create a new folder in **_config['OUTPUT_FOLDER_PATH']_** with name the indico_id of the camera video (in case of Academic Training) or Create a new folder in **_config['OUTPUT_FOLDER_PATH']_** with name of a random string (in case of E-Learning). (let's say **_folderB_** for this example)
- cd in **_folderA_**
- Run the ffmpeg commands
   - Academic Training Case:
        - Cut the camera and slides video according to the given presentation and questions time (**command-1**)
        - This will create the videos: **camera_presentation_part.mp4**, **slides_main_part.mp4**, **camera_main_part.mp4**, **camera_questions_part.mp4** in **_folderA_**
        - Run the ffmpeg **command-3**. The output of this command will be saved in **_folderB_** and in **_folderC_** *
    - E-Learning Case:
        - Run the (**command-2**). The output of this command will be saved in **_folderB_**
- Send the email confirmation
- Remove **_folderA_** and the containing files

 Which is **_folderC?_**
 Given the input URL: https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/2014/296743/296743_desktop_camera_1080p_4000.mp4 if you replace the string **config['URL_DOMAIN_INPUT_VIDEOS']** with the string **config['PIP_FOLDER_PATH']** and remove the string after the last backslash (_296743_desktop_camera_1080p_4000.mp4_) 















