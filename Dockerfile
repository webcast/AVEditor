FROM centos/python-36-centos7
# FROM centos/python-34-centos7

EXPOSE 8080

# Install yum packages with the ROOT user
USER root
RUN yum install -y epel-release

#install shh
RUN yum -y install openssh-server openssh-clients

ENV LD_LIBRARY_PATH /opt/rh/rh-python36/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
ENV PYTHONPATH /opt/app-root/src
ENV FLASK_APP /opt/app-root/src/wsgi.py

# Set the permissions for the app-user user
RUN chgrp -R 0 /opt/app-root && chmod -R ug+rwx /opt/app-root


RUN pip install --upgrade pip

USER 1001

WORKDIR /opt/app-root/src

# Install pip requirements
COPY requirements.txt /opt/app-root/src/requirements.txt

RUN pip install -r requirements.txt
#COPY ./requirements-dev.txt /opt/app-root/src/requirements-dev.txt
#RUN pip install -r requirements-dev.txt

COPY app /opt/app-root/src/app
COPY etc /opt/app-root/src/etc
COPY secret /opt/app-root/src/secret
COPY server /opt/app-root/src/server
COPY wsgi.py /opt/app-root/src/wsgi.py

CMD ["sh", "/opt/app-root/src/etc/entrypoint.sh"]
