from app.common.logger import setup_webapp_logs
from app.settings import BaseConfig
from app.app_factory import create_app
from app.cli import initialize_cli
from secret import config
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
This file is the one loaded on Openshift using Gunicorn.
"""

application = create_app(BaseConfig)

setup_webapp_logs(application, to_remote=application.config.get('LOG_REMOTE_ENABLED', False))
initialize_cli(application)
