$(document).ready(function(){

    var csrftoken = $('meta[name=csrf-token]').attr('content');

    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
    }
    });

    $('input[type="radio"]').click(function() {
        
        if ($(this).attr("value") == "academic_training") {
            $(".intro_outro_url").hide();
        }
        if ($(this).attr("value") == "e_learning") {
            $(".intro_outro_url").hide();
        }
        if ($(this).attr("value") == "other") {
            $(".intro_outro_url").show();
        }
      });

    // show crop options if crop checkbox is selected
    $("#crop_bool").change(function() {
        if(this.checked) {
            $("#crop_times").show();
        } else {
            $("#crop_times").hide();
        }
    });

    // Validate Go! form, returning on failure.
    $("#video_details_form").on("submit", function(event) {
        event.preventDefault();
        var validated = true;
        var camera_url = $('#camera_url').val();
        var slides_url = $('#slides_url').val();
        var intro_url = $('#intro_url').val();
        var outro_url = $('#outro_url').val();
   
        // other_choice of intro outro
        if ($('#intro_outro-2').is(':checked')) {
            if (intro_url == "" || outro_url == "") {
                $("#intro_outro_error").show();
                validated = false;
            }
        }
   
        // continuous_choice
        if ($('#pip-0').is(':checked')) {
            if (camera_url == '') {
                $("#camera_url_error").show();
                validated = false;
            } else {
                $("#camera_url_error").hide();
            }
            if (slides_url == '') {
                $("#slides_url_error").show();
                validated = false;
            } else {
                $("#slides_url_error").hide();
            }
        }
        // periodical_choice
        if ($('#pip-1').is(':checked')) {
            if (camera_url == '') {
                $("#camera_url_error").show();
                validated = false;
   
            } else {
                $("#camera_url_error").hide();
            }
            if (slides_url == '') {
                $("#slides_url_error").show();
                validated = false;
   
            } else {
                $("#slides_url_error").hide();
            }
        }
        // only_camera_choice
        if ($('#pip-2').is(':checked')) {
            if (camera_url == '') {
                $("#camera_url_error").show();
                validated = false;
            } else {
                $("#camera_url_error").hide();
            }
   
        }
        // only_slides_choice
        if ($('#pip-3').is(':checked')) {
   
            if (slides_url == '') {
                $("#slides_url_error").show();
                validated = false;
   
            } else {
                $("#slides_url_error").hide();
            }
        }
   
        if (validated) {
            var formData = new FormData(this);
            $.ajax({
                url: '/',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
            $("#tool").hide();
            $("#job_submitted").show();
            console.log('validated');
        } else {
            console.log('not validated');
        }
    });
   
    $('input[type="radio"][name="intro_outro"]').click(function() {
        if ($(this).attr("value") == "academic_training") {
            $(".intro_outro_url").hide();
        }
        if ($(this).attr("value") == "e_learning") {
            $(".intro_outro_url").hide();
        }
        if ($(this).attr("value") == "other") {
            $(".intro_outro_url").show();
        }
    });
   
    $('input[type="radio"][name="pip"]').click(function() {
        var camera_url = $('#camera_url').val();
        var slides_url = $('#slides_url').val();
   
        if ($(this).attr("value") == "continuous_choice") {
            if (camera_url == '') {
                $("#camera_url_error").show();
            } else {
                $("#camera_url_error").hide();
            }
            if (slides_url == '') {
                $("#slides_url_error").show();
            } else {
                $("#slides_url_error").hide();
            }
        }
        if ($(this).attr("value") == "periodical_choice") {
            if (camera_url == '') {
                $("#camera_url_error").show();
            } else {
                $("#camera_url_error").hide();
            }
            if (slides_url == '') {
                $("#slides_url_error").show();
            } else {
                $("#slides_url_error").hide();
            }
        }
        if ($(this).attr("value") == "only_camera_choice") {
            if (camera_url == '') {
                $("#camera_url_error").show();
            } else {
                $("#camera_url_error").hide();
            }
            $("#slides_url_error").hide();
        }
        if ($(this).attr("value") == "only_slides_choice") {
            if (slides_url == '') {
                $("#slides_url_error").show();
            } else {
                $("#slides_url_error").hide();
            }
            $("#camera_url_error").hide();
        }
    });

}); // end jquery


// tab function
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// regex to match HH.MM.SS
var checkTime = function(time){
    var re = new RegExp('([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]');
    return re.test(time)
};

var convert_second_to_timestamp = function(seconds){
    d = parseInt(seconds);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
};

var time_is_within_limits = function(time1, time2){
    return time1 <= time2;
};

var secondsToHms = function(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
};

var reset_form = function(){
    $("#video_filename").find('span').text("");
    $("#video_filename2").find('span').text("");
    $("#crop_start, #crop_end").val("");
};