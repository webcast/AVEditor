from secret import config


class OauthConfig(object):
    """
    CERN Oauth configuration
    """
    CERN_OAUTH_CLIENT_ID = config.CERN_OAUTH_CLIENT_ID
    CERN_OAUTH_CLIENT_SECRET = config.CERN_OAUTH_CLIENT_SECRET
    CERN_OAUTH_AUTHORIZE_URL = config.CERN_OAUTH_AUTHORIZE_URL
    CERN_OAUTH_TOKEN_URL = config.CERN_OAUTH_TOKEN_URL

class FlaskUploads(object): 
    UPLOADED_IMAGES_DEST = "/opt/app-root/app/upload_folder/"
    UPLOADED_VIDEO_DEST = "/opt/app-root/app/upload_folder/"
    UPLOADS_DEFAULT_DEST = "/opt/app-root/app/upload_folder/"
    UPLOADED_VIDEO_URL = "/opt/app-root/app/upload_folder/"
    VIDEO_OUTPUT_DEST = "/opt/app-root/app/output_folder/"

class Mailer:
    MAIL_HOSTNAME = "http://cernmx.cern.ch/"
    MAIL_FROM = "aveditor@aveditor.web.cern.ch"
    MAIL_CONFIRM_WIN_PATH = "G:\\Services\\MediaArchive\\DropFolder\\"
    MAIL_CONFIRM_OTHER_PATH = "https://dfsweb.web.cern.ch/dfsweb/Services/DFS/DFSBrowser.aspx/Services/MediaArchive/DropFolder/"
    MAIL_CONFIRM_FINAL_LOCATION = "https://dfsweb.web.cern.ch/dfsweb/Services/DFS/DFSBrowser.aspx/Services/"

class IntroOutroWebsites:
    E_LEARNING_INTRO = "https://twiki.cern.ch/twiki/pub/ELearning/WebHome/First_slide_with_music.mp4"    
    E_LEARNING_OUTRO = "https://twiki.cern.ch/twiki/pub/ELearning/WebHome/last_three_slides.mp4"    
    ACADEMIC_TRAINING_INTRO = "https://twiki.cern.ch/twiki/pub/Edutech/CernAcademicTrainingInYouTube/IntroVideoLectures-FHD-audio.mp4"    
    ACADEMIC_TRAINING_OUTRO = "https://twiki.cern.ch/twiki/pub/Edutech/CernAcademicTrainingInYouTube/OutroVideoLectures-FHD-audio.mp4"

class FFMPEG(object):
    FFMPEG_BIN = "/usr/bin/ffmpeg"
    FFMPEG_FILE_FOLDER = "/mnt/aveditor"

class SSH:
    SSH_KEY = "/opt/app-root/src/secret/neoapi_rsa"
    SSH_USERNAME = "root"
    SSH_SERVER = "handbrake05.cern.ch"
    SSH_PORT = "22"

class OutputVideos():
    URL_DOMAIN_INPUT_VIDEOS = "https://mediastream.cern.ch/"
    PIP_FOLDER_PATH = "/mnt/"
    OUTPUT_FOLDER_PATH = "/mnt/master_output/"
    PIP_OUTPUT_FILENAME = "podcast_master_new.mp4"

class DatabaseConfig(object):
    """
    Application database configuration
    """
    # FROM_HERE Comment when run in localhost
    DB_NAME = config.DB_NAME
    DB_PASS = config.DB_PASS
    DB_PORT = config.DB_PORT
    DB_SERVICE = config.DB_SERVICE
    DB_USER = config.DB_USER

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = False
    SQLALCHEMY_POOL_SIZE = config.SQLALCHEMY_POOL_SIZE
    SQLALCHEMY_POOL_TIMEOUT = config.SQLALCHEMY_POOL_TIMEOUT
    SQLALCHEMY_POOL_RECYCLE = config.SQLALCHEMY_POOL_RECYCLE
    SQLALCHEMY_MAX_OVERFLOW = config.SQLALCHEMY_MAX_OVERFLOW
    # TO_HERE

    """
    Database URI will be generated with the already gotten database parameters
    """
    # FROM_HERE Uncomment when run in localhost

    # SQLALCHEMY_DATABASE_URI =  'sqlite:////opt/app-root/transcode.db'

    # TO_HERE

    # FROM_HERE Comment when run in localhost
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    # TO_HERE
        
class BaseConfig(OauthConfig, DatabaseConfig, FlaskUploads, Mailer, IntroOutroWebsites, FFMPEG, SSH, OutputVideos):
    """
    Stores the app configuration
    """

    ADMIN_EGROUP = config.ADMIN_EGROUP
    APP_PORT = config.APP_PORT
    SECRET_KEY = config.SECRET_KEY

    """
    Debug and logging configuration
    """
    DEBUG = config.DEBUG
    IS_LOCAL_INSTALLATION = config.IS_LOCAL_INSTALLATION

    LOG_LEVEL = config.LOG_LEVEL
    # Add the configuration for remote logging
    # This information should be present in the configmap and also on the environment variables
    LOG_REMOTE_ENABLED = True
    LOG_REMOTE_URL = config.LOG_REMOTE_URL
    LOG_REMOTE_TYPE = config.LOG_REMOTE_TYPE
    LOG_REMOTE_PRODUCER = config.LOG_REMOTE_PRODUCER

    """
    Whether or not to use the wsgi Proxy Fix
    """
    USE_PROXY = config.USE_PROXY
    try:
        SERVER_NAME = config.SERVER_NAME
    except AttributeError:
        pass
