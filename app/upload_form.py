from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileRequired
from wtforms import FieldList, HiddenField, IntegerField
from wtforms.fields import BooleanField, StringField, FormField, FileField, RadioField
from wtforms.validators import DataRequired, Optional, NumberRange

from app.extensions import images, video, time_regex


# custom validator to validate on condition
class RequiredIf(DataRequired):
    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(RequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        # if other_field contains data then "field" will be required
        if bool(other_field.data):
            super(RequiredIf, self).__call__(form, field)


# custom validator to validate on condition
# will only require
class ImageRequiredIf(DataRequired):
    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(ImageRequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        print(other_field.data)
        if other_field.data:
            super(ImageRequiredIf, self).__call__(form, field)


class ImageForm(FlaskForm):
    img_upload = FileField(u'Video File', validators=[FileAllowed(images, 'Images only!')])
    duration = IntegerField("Pic Stop", validators=[ImageRequiredIf('img_upload'), Optional(), NumberRange()])
    slide_type = RadioField('Slide Type', choices=[('intro','Intro slide'),('outro','Outro slide')], default='intro')


class UploadForm(FlaskForm):
    from_presentation = StringField('from_presentation')
    to_presentation = StringField('to_presentation')
    from_questions = StringField('from_questions')
    to_questions = StringField('to_questions')
    from_crop = StringField('from_crop')
    to_crop = StringField('to_crop')
    slides_url = StringField('slides_url')
    camera_url = StringField('camera_url')
    intro_url = StringField('intro_url')
    outro_url = StringField('outro_url')
    pip = RadioField('choice',
        choices=[('continuous_choice', 'continuous'), ('periodical_choice', 'for 20 seconds every 100 seconds'), 
        ('only_camera_choice', 'only camera'),
        ('only_slides_choice', 'only slides')], default='continuous_choice')
    intro_outro = RadioField('intro_outro_choice',
        choices=[('academic_training', 'Academic Training'), ('e_learning', 'E-Learning'), 
        ('other', 'Other')], default='academic_training')



class VideoForm(FlaskForm):
    # video upload form
    file = FileField(u'Video File', validators=[FileRequired(), FileAllowed(video, 'Videos only!')])
