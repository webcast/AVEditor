 # coding=utf8
import logging
import glob
import os
import random
import uuid
import sys
import re
import paramiko

logger = logging.getLogger('webapp.utils')

from flask import current_app, url_for

def str2bool(v):
    """
    Converts a string to boolean

    :param v: Value to convert to boolean
    :return: The boolean result
    """
    return v.lower() in ("yes", "true", "t", "1")

def init_ssh_client():
    """
    Initializes and ssh connection to a remote machine
    :return: the ssh client
    """
    ssh = None
    try:
        logger.info("Initializing SSH client")
        ssh = paramiko.SSHClient()
        # automatically adds host to known hosts list
        k = paramiko.RSAKey.from_private_key_file(current_app.config["SSH_KEY"])
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        logger.debug("CONNECTING at {}@{}".format('root', 'handbrake05.cern.ch'))
        ssh.connect('handbrake05.cern.ch', username=current_app.config["SSH_USERNAME"], pkey=k)
        logger.debug('Connected to the SSH client')
    except Exception as e:
        logger.error("failled to init ssh client: {}".format(e))
    return ssh

def ffmpeg_cmd(originpath):
    """
    prepare the ffmpeg command from the given originpath
    :param originpath: can use only originpath on DFS including the file  name
    :return: full command line which will be exectuted on the remote machine.
    """
    mount_point= '/mnt/origin/Rooms'
    root_origin_path = '\\cern.ch\\dfs\\Services\\Recordings\\Rooms\\'
    ffmpeg_transcode = "./ffmpeg -i {} -y -c:a libfdk_aac -b:a 96k -c:v libx264 -s 1920x1080 -crf 21 -preset medium {} -nostdin -nostats > output.log 2>&1 < /dev/null &"

    #searching for camera or slides.mp4
    regex = r".{6}\.mp4"
    try:
        relative_path = (originpath.split(root_origin_path)[1]).replace('\\','/')
        logger.debug("relative_path file path: {}".format(relative_path))
        input_file_path = os.path.join(mount_point,relative_path)
        logger.debug("input file path: {}".format(input_file_path))
        sub_path  = re.sub(regex,'',relative_path)
        logger.debug("sub_path file path: {}".format(sub_path))
        output_file_path = os.path.join(mount_point,sub_path,"transcoded_ffmpeg.mp4" )
        logger.debug("output file path: {}".format(output_file_path))
        cmd = ffmpeg_transcode.format(input_file_path, output_file_path)
        logger.info("ffmpeg command to execute is : {}".format(cmd))
        return cmd

    except Exception as e:
        logger.error("failled to create fffmpeg command with error: {}".format(e))
        return None

def ffmpeg_run(cmd):
    """
    WIll run a specified command over SSH
    :param cmd: The command to be executed on the remote machine
    :return: A JSON object containing the stdout of the executed command, and the success flag
    """
    try:
        ssh = init_ssh_client()
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
        logger.debug("Executed cmd: %s \n", cmd)
        # if ssh return success code: 0
        if ssh_stdout.channel.recv_exit_status() == 0:
            output = ""
            # ssh command returns an array containing the JSON output,
            # parse JSON
            for line in ssh_stdout.readlines():
                output += line.rstrip()
            ssh.close()
            if is_json(output):
                output = json.loads(output)
                output['success'] = True
                return output
            return {"success": True}
        current_app.logger.error('FFmpeg error\n')
        ssh.close()
    except:
        return {"failed": False}
    return {"success": False}

def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True

def get_intro_outro_urls(form):
    intro_outro_choice = form.data['intro_outro']

    if intro_outro_choice == 'academic_training':
        intro_url = current_app.config['ACADEMIC_TRAINING_INTRO']
        outro_url = current_app.config['ACADEMIC_TRAINING_OUTRO']
    elif intro_outro_choice == 'e_learning':
        intro_url = current_app.config['E_LEARNING_INTRO']
        outro_url = current_app.config['E_LEARNING_OUTRO'] 
    else:
        intro_url = form.data['intro_url']
        outro_url = form.data['outro_url']

    return intro_url, outro_url

def find_output_path(url):
    # input: https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/2014/296743/296743_desktop_camera_1080p_4000.mp4
    # output: /mnt/MediaArchive/Video/Master/WebLectures/2014/296743/media/

    return url.replace(current_app.config['URL_DOMAIN_INPUT_VIDEOS'], current_app.config['PIP_FOLDER_PATH']) \
                                       .replace(url.split('/')[-1], "media/").replace("Public", "Master")


def get_academic_training_output_filename(url):
    # input: https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/2014/296743/296743_desktop_camera_1080p_4000.mp4
    # output: /mnt/MediaArchive/Video/Master/WebLectures/2014/296743/media/current_app.config['PIP_OUTPUT_FILENAME']

    return os.path.join(url.replace(current_app.config['URL_DOMAIN_INPUT_VIDEOS'], \
                                    current_app.config['PIP_FOLDER_PATH']).replace(url.split('/')[-1], "media/").replace("Public", "Master"), \
                                    current_app.config['PIP_OUTPUT_FILENAME'])

def get_elearning_output_filename(url):
    # input: https://indico.cern.ch/event/indico_id/whatever/filename.mp4
    # output: /mnt/master_output/indico_id/$random_query$_preview.mp4
    
    return os.path.join(current_app.config['OUTPUT_FOLDER_PATH'], \
                        url.split('/')[-2], \
                        '_' + uuid.uuid4().hex.lower()[0:6] + '_preview.mp4')

def get_indico_id(url):
    # input: https://mediastream.cern.ch/MediaArchive/Video/Public/WebLectures/2014/296743/296743_desktop_camera_1080p_4000.mp4
    # output: 296743

    return url.split('/')[-2]

def send_confirmation_email_cmd_at(user_email, output_path, full_filename):
    """
    Will generate the mail confirmation command for Academic Training Videos 
    """

    windows_path = output_path.replace(current_app.config['OUTPUT_FOLDER_PATH'], current_app.config['MAIL_CONFIRM_WIN_PATH']) + "\\" + \
                   current_app.config['PIP_OUTPUT_FILENAME']
    other_path = os.path.join((output_path.replace(current_app.config['OUTPUT_FOLDER_PATH'], current_app.config['MAIL_CONFIRM_OTHER_PATH'])), \
                 current_app.config['PIP_OUTPUT_FILENAME'])
    final_location = full_filename.replace(current_app.config['PIP_FOLDER_PATH'], current_app.config['MAIL_CONFIRM_FINAL_LOCATION']). \
                                   replace("Master", "Masters")

    subject = "AVEditor confirmation email"
    body_message = "Your video has been processed successfully. \n" + \
                   "\n" + \
                   "You can find it in the following floder path: \n" + \
                   "Preview for Windows: " + windows_path + "\n" + \
                   "Preview for Other OS: " + other_path + "\n" + \
                   "\n" + \
                   "Final location: " + final_location + "\n" + \
                   "\n" + \
                   "aveditor\n" 

    return " echo -e " + '"' + body_message + '"' + \
           " | mailx -s smtp=" + current_app.config['MAIL_HOSTNAME'] + \
           " -r " + current_app.config['MAIL_FROM'] + \
           " -s " + '"' + subject +  '"' +\
           " -v " + user_email


def send_confirmation_email_cmd_el(user_email, output_path, full_filename):
    """
    Will generate the mail confirmation command for E-Learning Videos
    """
    
    filename = full_filename.split('/')[-1]

    windows_path = output_path.replace(current_app.config['OUTPUT_FOLDER_PATH'], current_app.config['MAIL_CONFIRM_WIN_PATH']) + "\\" + \
                   filename
    other_path = os.path.join((output_path.replace(current_app.config['OUTPUT_FOLDER_PATH'], current_app.config['MAIL_CONFIRM_OTHER_PATH'])), \
                   filename)

    subject = "AVEditor confirmation email"
    body_message = "Your video has been processed successfully. \n" + \
                   "\n" + \
                   "You can find it in the following floder path: \n" + \
                   "Preview for Windows: " + windows_path + "\n" + \
                   "Preview for Other OS: " + other_path + "\n" + \
                   "\n" + \
                   "aveditor\n"

    return " echo -e " + '"' + body_message + '"' + \
           " | mailx -s smtp=" + current_app.config['MAIL_HOSTNAME'] + \
           " -r " + current_app.config['MAIL_FROM'] + \
           " -s " + '"' + subject +  '"' +\
           " -v " + user_email


def send_error_notification_email_cmd(user_email, error_message):
    subject = "AVEditor error notification"
    body_message = "Unfortunately your request was not completed. \n" + \
                   "This is the last error message:\n" + \
                   "\n" + \
                   error_message

    return " echo -e " + "'" + body_message + "'" + \
           " | mailx -s smtp=" + current_app.config['MAIL_HOSTNAME'] + \
           " -r " + current_app.config['MAIL_FROM'] + \
           " -s " + '"' + subject + '"' + \
           " -v " + user_email