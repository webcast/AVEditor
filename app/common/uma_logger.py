"""
Python logger that sends to UMA
"""
from logging import StreamHandler
from threading import Thread
import time
import json
import socket

import requests


class UMAHandler(StreamHandler):
    """ Logger that sends to UMA """
    def __init__(self, producer, my_type, bulk_size=10, threaded=True):
        StreamHandler.__init__(self)
        self.messages = []
        self.producer = producer
        self.type = my_type
        self.threads = []
        self.bulk_size = bulk_size
        self.threaded = threaded

    def emit(self, record):
        """ Store a message in a list, to send them in bulk"""
        current_milli_time = lambda: int(round(time.time() * 1000))
        my_entry = {
            'raw': record.msg,
            'level': record.levelname,
            'type': self.type,
            'producer': self.producer,
            'timestamp': str(current_milli_time()),
            'lineno': record.lineno,
            'pathname': record.pathname,
            'funcName': record.funcName,
            'hostname': socket.gethostname(),
            'name': record.name,
            'message': record.message
        }
        self.messages.append(my_entry)
        if len(self.messages) > self.bulk_size:
            self.flush()

    def close(self):
        """ Harvest all the threads"""
        for my_thread in self.threads:
            my_thread.join()

    def flush_threaded(self, my_messages): #pylint: disable= no-self-use
        """ Send data to the unified monitoring infrastructure"""
        print("Sending logs to monitoring...")
        response = requests.post('http://monit-logs.cern.ch:10012/',
                                 data=json.dumps(my_messages),
                                 headers={"Content-Type":
                                          "application/json; charset=UTF-8"},
                                 timeout=3)

        if not response.status_code in [200]:
            print("Error sending the information to UMA")

    def flush(self):
        """ Send the messages in a bulk, and check if any previous threads
            have already finnished """
        for my_thread in self.threads:
            if not my_thread.is_alive():
                my_thread.join()
                self.threads.remove(my_thread)

        if self.threaded:
            new_thread = Thread(target=self.flush_threaded,
                                args=([self.messages]))
            new_thread.start()
            self.threads.append(new_thread)
        else:
            self.flush_threaded(self.messages)
        self.messages = []
