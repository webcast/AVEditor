from logging.handlers import RotatingFileHandler
from datetime import datetime
import logging
from pytz import timezone, utc
import sys
from app.common.uma_logger import UMAHandler

def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_webapp_logs(app, to_stdout=True, to_remote=False):
    logger = logging.getLogger('webapp')

    if app.config['LOG_LEVEL'] == 'DEV':
        logger.setLevel(logging.DEBUG)

    if app.config['LOG_LEVEL'] == 'PROD':
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(levelname)s - %(asctime)s - %(name)s - %(message)s - %(pathname)s - %(funcName)s():%(lineno)d')

    logging.Formatter.converter = zurich_time

    if to_stdout:
        print("Logging to stdout -> True")
        configure_stdout_logging(logger=logger, formatter=formatter, log_level=app.config['LOG_LEVEL'])

    if to_remote:
        print("Logging to remote -> True")
        configure_remote_logging(logger=logger, app=app)


def configure_remote_logging(logger=None, app=None, formatter=None):

    handler = UMAHandler(app.config.get('LOG_REMOTE_PRODUCER', None), app.config.get('LOG_REMOTE_TYPE', None))

    print("Configuring remote logging with producer: {} and type {}".format(app.config.get('LOG_REMOTE_PRODUCER', None), app.config.get('LOG_REMOTE_TYPE', None)))

    handler.setFormatter(formatter)

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception

    logger.addHandler(handler)

    if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
        handler.setLevel(logging.DEBUG)

    if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
        handler.setLevel(logging.INFO)

    logger.addHandler(handler)


def configure_stdout_logging(logger=None, formatter=None, log_level="DEV"):
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if log_level == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)