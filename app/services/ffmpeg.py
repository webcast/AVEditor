import os
import shutil
import paramiko
import logging
import subprocess, shlex
from app.services.celery_tasks import run_ffmpeg, clean
from app.common.utils import get_intro_outro_urls, get_elearning_output_filename, get_academic_training_output_filename, get_indico_id, \
                         send_confirmation_email_cmd_at, send_confirmation_email_cmd_el, send_error_notification_email_cmd, init_ssh_client
from flask import json, current_app
from celery import chain

from app.extensions import setup_custom_logger

logger = logging.getLogger('webapp.ffmpeg')

class FfmpegTaskRunner:

    def __init__(self):
        self.ffmpeg_bin = current_app.config['FFMPEG_BIN']
        self.ffmpeg_file_folder = current_app.config['FFMPEG_FILE_FOLDER']

        # self.output_video_codec = " -c:a aac -c:v libx264 -b:v 2000k -s 1920x1080 -r 25 "
        self.output_video_codec = " -strict -2 "

    def syscom(self, cmd):
        """
            Execute a process
        """
        self.logger.debug("Syscom: %s" % subprocess.list2cmdline(cmd))
        p = subprocess.Popen(cmd, False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, _ = p.communicate()
        return out

    def ffmpeg_gen_video(self, form, current_user):
        """
        Will generate the final video
        :param form: client POST form
        """
        logger.info('Starting ffmpeg video generation')
        cmd_folder_preprocess = "cd " + current_app.config['FFMPEG_FILE_FOLDER'] + " && directory=$(date +%s) && mkdir $directory && cd $directory && "
        cmd_folder_postprocess = " && cd .. && rm -r $directory -f "
        final_command = ""

        intro_url, outro_url = get_intro_outro_urls(form)
        pip_choice = form.data['pip']

        if pip_choice == 'continuous_choice' or pip_choice == 'periodical_choice':
            logger.info('Selected video is continuous or periodical')
            camera_url = form.data['camera_url']
            output_folder = os.path.join(current_app.config['OUTPUT_FOLDER_PATH'], get_indico_id(camera_url))
            full_filename = get_academic_training_output_filename(camera_url)

            final_command = self.ffmpeg_commands_with_pip(form)
            cmd_folder_postprocess = " && " + send_confirmation_email_cmd_at(current_user['email'], output_folder, full_filename) + cmd_folder_postprocess
        elif pip_choice == 'only_camera_choice':
            logger.info('Selected video is only camera')
            camera_url = form.data['camera_url']
            output_folder = os.path.join(current_app.config['OUTPUT_FOLDER_PATH'], get_indico_id(camera_url))
            full_filename = get_elearning_output_filename(camera_url)

            cmd_concat_videos = self.ffmpeg_concat_video(intro_url, camera_url, outro_url, full_filename)
            final_command += cmd_concat_videos
            cmd_folder_postprocess = " && " + send_confirmation_email_cmd_el(current_user['email'], output_folder, full_filename) + cmd_folder_postprocess
        elif pip_choice == 'only_slides_choice':
            logger.info('Selected video in only slides')
            slides_url = form.data['slides_url']
            output_folder = os.path.join(current_app.config['OUTPUT_FOLDER_PATH'], get_indico_id(slides_url))
            full_filename = get_elearning_output_filename(slides_url)

            cmd_concat_videos = self.ffmpeg_concat_video(intro_url, slides_url, outro_url, full_filename)
            final_command += cmd_concat_videos
            cmd_folder_postprocess = " && " + send_confirmation_email_cmd_el(current_user['email'], output_folder, full_filename) + cmd_folder_postprocess

        cmd_folder_preprocess += " mkdir -p " + output_folder + " && "
        final_command = cmd_folder_preprocess + final_command + cmd_folder_postprocess

        # self.ffmpeg_run_local(final_command)
        print(final_command)
        logger.info('Sending command to the remote machine')
        logger.info("Command to be sent: " + final_command)

        result = self.ffmpeg_run(final_command)

        if result['success'] == True:
            logger.info("Remote command run successfully")
        else:
            self.ffmpeg_run(send_error_notification_email_cmd(current_user['email'], result['error_message']))
            logger.error("Error notification email sent")
            logger.error("This is the error email sent:" + send_error_notification_email_cmd(current_user['email'], result['error_message']))

    def ffmpeg_commands_with_pip(self, form):
        intro_url, outro_url = get_intro_outro_urls(form)
        from_presentation_time = form.data['from_presentation']
        to_presentation_time = form.data['to_presentation']
        from_questions_time = form.data['from_questions']
        to_questions_time = form.data['to_questions']
        camera_url = form.data['camera_url']
        slides_url = form.data['slides_url']
        videos_before_pip = [intro_url]
        videos_after_pip = []
        videos_for_pip = []

        cmd_cut_camera = ""
        cmd_cut_slides = ""
        final_command = ""

        output_folder = os.path.join(current_app.config['OUTPUT_FOLDER_PATH'], get_indico_id(camera_url))

        if (to_presentation_time > from_presentation_time):
            cmd_cut_camera += self.ffmpeg_cut_video(camera_url, from_presentation_time, to_presentation_time, "camera_presentation_part.mp4") + " && "
            videos_before_pip.append("camera_presentation_part.mp4")
        if (to_questions_time > from_questions_time):
            cmd_cut_camera += self.ffmpeg_cut_video(camera_url, from_questions_time, to_questions_time, "camera_questions_part.mp4") + " && "
            videos_after_pip.append("camera_questions_part.mp4")

        videos_after_pip.append(outro_url)

        if (from_questions_time > to_presentation_time):
            cmd_cut_camera += self.ffmpeg_cut_video(camera_url, to_presentation_time, from_questions_time, "camera_main_part.mp4")
            cmd_cut_slides += self.ffmpeg_cut_video(slides_url, to_presentation_time, from_questions_time, "slides_main_part.mp4")
            videos_for_pip.append("slides_main_part.mp4")
            videos_for_pip.append("camera_main_part.mp4")
            final_command += cmd_cut_camera + " && " + cmd_cut_slides + " && "
        else:
            videos_for_pip.append(slides_url)
            videos_for_pip.append(camera_url)

        full_filename = get_academic_training_output_filename(camera_url)
        enable_time_bool = True if form.data['pip'] == 'periodical_choice' else False
        cmd_pip_and_concat = self.ffmpeg_pip_and_concat_video(videos_before_pip, videos_for_pip, videos_after_pip, full_filename, enable_time_bool)

        final_command += cmd_pip_and_concat
        # copy the output file to config['OUTPUT_FOLDER_PATH']/indico_id
        final_command += " && cp " + full_filename + " " + output_folder

        return final_command

    # An example of the generated command in case we have presentation part and wuestions part
    # ffmpeg -y -i intro.mp4 \
    # -i camera_presentation_part.mp4 \
    # -i slides_main_part.mp4 \
    # -i camera_main_part.mp4 \
    # -i camera_questions_part.mp4 \
    # -i outro.mp4 \
    # -filter_complex " \
    # [0:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0]; \
    # [1:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v1]; \
    # [2:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9,fps=fps=25[v2]; \
    # [3:v] scale=iw/3:ih/3 [v3]; \
    # [4:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v4]; \
    # [5:v] scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v5]; \
    # [you can use only one of the below two lines, depending on your case
    # [v2][v3] overlay=main_w-overlay_w-10:main_h-overlay_h-10[outv]; \ [CONTINUOUS PIP]
    # [v2][v3] overlay=main_w-overlay_w-10:main_h-overlay_h-10:enable='lt(mod(t,120),20)'[outv]; \ [PERIODICAL PIP]
    # ]
    # [v0][0:a][v1][1:a][outv][3:a][v4][4:a][v5][5:a] concat=n=5:v=1:a=1[v][a]" -map "[v]" -map "[a]" final_video.mp4
    def ffmpeg_pip_and_concat_video(self, videos_to_concat_before_pip, videos_for_pip, videos_to_concat_after_pip, output_filename, enable_time_bool):
        """
        Will generate the ffmpeg command in order to create a pip video
        It can generate the correct command even if there is presentation part or not, even if
        there is questions part or not.
        """
        # using this argument if you want to have periodical PiP
        enable_time_arg = ":enable='lt(mod(t,120),20)'" if enable_time_bool else ""
        video_filter = "[{}:v] scale=1920:1080:force_original_aspect_ratio=1,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v{}]; "
        video_filter_pip = ["[{}:v] scale=1920:1080:force_original_aspect_ratio=1,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9,fps=fps=25[v{}]; ", \
                            "[{}:v] scale=1920:1080,scale=iw/3:ih/3 [v{}]; "]
        overlay_filter = "[v{}][v{}] overlay=main_w-overlay_w-10:main_h-overlay_h-10"
        video_audio_part = "[v{}][{}:a]"
        input_videos = ""
        concat_part = ""

        filter_complex_cmd = " -filter_complex" + ' " '

        current_video_no = 0
        for vd in videos_to_concat_before_pip:
            input_videos += " -i " + vd
            filter_complex_cmd += video_filter.format(current_video_no, current_video_no)
            concat_part += video_audio_part.format(current_video_no, current_video_no)
            current_video_no += 1

        i = 0
        for vd in videos_for_pip:
            input_videos += " -i " + vd
            filter_complex_cmd += video_filter_pip[i].format(current_video_no, current_video_no)
            current_video_no += 1
            i += 1
        concat_part += "[outv][{}:a]".format(current_video_no-1)
        overlay_filter = overlay_filter.format(current_video_no-2, current_video_no-1)

        for vd in videos_to_concat_after_pip:
            input_videos += " -i " + vd
            filter_complex_cmd += video_filter.format(current_video_no, current_video_no)
            concat_part += video_audio_part.format(current_video_no, current_video_no)
            current_video_no += 1

        concat_part += " concat=n={}:v=1:a=1[v][a]".format(current_video_no-1)

        return self.ffmpeg_bin + \
            " -y " + \
            input_videos + \
            filter_complex_cmd + \
            overlay_filter + enable_time_arg + "[outv]; " + \
            concat_part + \
            ' " ' + \
            ' -strict -2 -map "[v]" -map "[a]" ' + \
            self.output_video_codec + \
            output_filename

    # An example of the generated command
    # ffmpeg -i intro.mp4 -i try.mp4 -i intro.mp4
    #  -filter_complex \
    # "[0:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0];
    #  [1:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v1];
    #  [2:v]scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v2];\
    #  [v0][0:a][v1][1:a][v2][2:a] concat=n=3:v=1:a=1[v][a]" \
    # -map "[v]" -map "[a]" final_video.mp4
    def ffmpeg_concat_video(self, intro_video, main_video, outro_video, output_filename):
        return self.ffmpeg_bin + \
            " -y " + \
            "-i " + intro_video + " " + \
            "-i " + main_video + " " + \
            "-i " + outro_video + " " + \
	        "-filter_complex" + \
            ' " ' + \
            "[0:v]scale=1920:1080:force_original_aspect_ratio=1,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v0]; " + \
            "[1:v]scale=1920:1080:force_original_aspect_ratio=1,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v1]; " + \
            "[2:v]scale=1920:1080:force_original_aspect_ratio=1,pad=1920:1080:(ow-iw)/2:(oh-ih)/2,setdar=16/9[v2]; " + \
            "[v0][0:a][v1][1:a][v2][2:a] concat=n=3:v=1:a=1[v][a] " + \
            ' " ' + \
            '-strict -2 -map "[v]" -map "[a]" ' + \
            self.output_video_codec + \
            output_filename

    def ffmpeg_cut_video(self, input_url, from_time, to_time, output_name):
        """
        Will generate the ffmpeg command in order to cut a part of a video
        """
        return self.ffmpeg_bin + \
            " -y " + \
            "-i " + input_url + " " + \
            "-ss " + from_time + " " + \
            "-to " + to_time + " " + \
            self.output_video_codec + \
            output_name

    def ffmpeg_run_local(self, cmd):
        task = (run_ffmpeg.si(cmd))()

    def ffmpeg_run(self, cmd):
        """
        WIll run a specified command over SSH
        :param cmd: The command to be executed on the remote machine
        """
        # if os.environ['DEBUG']:
        #     self.logger.debug("Executed ffmpeg cmd on local container: %s \n", cmd)
        #     return self.ffmpeg_run_local(cmd)
        ssh = init_ssh_client()
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
        # if ssh return success code: 0
        if ssh_stdout.channel.recv_exit_status() == 0:
            output = ""
            # ssh command returns an array containing the JSON output,
            # parse JSON
            for line in ssh_stdout.readlines():
                output += line.rstrip()
            ssh.close()
            if self.is_json(output):
                output = json.loads(output)
                output['success'] = True
                return output
            logger.info('SSH command is sent successfully')
            return {"success": True}
        logger.error('SSH exec command failed')

        err = ""
        for line in ssh_stderr.readlines():
            err += line.rstrip()
        ssh.close()

        error = {"success": False, "error_message": err}
        return error

    def is_json(self, myjson):
        try:
            json.loads(myjson)
        except ValueError:
            return False
        return True
