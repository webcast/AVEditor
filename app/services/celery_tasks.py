import os
import shutil
import logging
import time
from celery import Celery
import subprocess, shlex
from app.extensions import setup_custom_logger

env=os.environ
CELERY_BROKER_URL=env.get('CELERY_BROKER_URL','redis://redis:6379'),
CELERY_RESULT_BACKEND=env.get('CELERY_RESULT_BACKEND','redis://redis:6379')


celery= Celery('celery_tasks',
                broker=CELERY_BROKER_URL,
                backend=CELERY_RESULT_BACKEND)

logger = logging.getLogger('webapp.celery')

def syscom(cmd):
    """
        Execute a process
    """
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    proc_stdout = p.communicate()[0].strip()
    return proc_stdout

@celery.task(name='ffmpeg.run_ffmpeg')
def run_ffmpeg(cmd):
    args = shlex.split(cmd)
    # exec_stdout = syscom(args)
    exec_stdout = syscom(cmd)
    logger.info("exec_stdout got: %s \n",exec_stdout)
    #return exec_stdout
    return {"success": True}


@celery.task(name='ffmpeg.clean')
def clean(url):
    """
    This function deleted files and DB entries in case of erros in the processing of the video
    """
    shutil.rmtree(url)
    os.remove(url)

