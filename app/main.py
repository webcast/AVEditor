#!flask/bin/python
from app.app_factory import create_app
from app.settings import BaseConfig
from app.common.logger import setup_webapp_logs
from app.extensions import setup_custom_logger

from app.cli import initialize_cli


app = create_app(BaseConfig)
setup_webapp_logs(app, to_file=False, webapp_log_path="")

logger = setup_custom_logger('root')

initialize_cli(app)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=app.config['APP_PORT'], debug=app.config['DEBUG'], ssl_context='adhoc')
