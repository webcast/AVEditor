from wtforms import StringField
from flask_wtf import FlaskForm


class MasterPathForm(FlaskForm):
    originpath = StringField('')
