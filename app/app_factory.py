from flask_wtf import CSRFProtect
from flask_uploads import configure_uploads
from app.common.authentication.cern_oauth import load_cern_oauth
from werkzeug.contrib.fixers import ProxyFix
from flask import Flask
from app.views.index_page import index_page
from app.extensions import db, images, video
from app.video_upload import video_upload

def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    # app.debug = True
    configure_uploads(app, (video))
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    # app.config.update(dict(
    #     SECRET_KEY="powerful secretkey",
    #     WTF_CSRF_SECRET_KEY="a csrf secret key"
    # ))

    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)


    # enable CSRF for WTF-forms
    CSRFProtect(app)

    db.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(index_page)
    app.register_blueprint(video_upload)

    load_cern_oauth(app)

    return app
