import os
import shutil
import logging
from flask import Blueprint, render_template, request, redirect, url_for, session, current_app
from app.forms import MasterPathForm
from app.common import utils
from app.extensions import video, images, db, setup_custom_logger
from app.services.ffmpeg import FfmpegTaskRunner 
from app.upload_form import UploadForm, VideoForm
from app.video_upload import video_upload

logger = logging.getLogger('webapp.views')

index_page = Blueprint('index_page', __name__,
                       template_folder='templates')

@index_page.route('/result', methods = ['POST', 'GET'])
def result():
   if request.method == 'POST':
      result = request.form
      return render_template("result.html",result = result)

@index_page.route('/', methods=['GET', 'POST'])
def home():
    logger.info('Loading home page')

    current_user = session.get('user', None)

    form = UploadForm()  # main form
    video_form = VideoForm()  # video form

    if request.method == 'POST':
        logger.info('Received new video request')
        logger.debug('Received a request DATA: ' + str(form.data))
        # validate main form
        if form.validate_on_submit():
            print(request.form)

            task_runner = FfmpegTaskRunner()
            task_runner.ffmpeg_gen_video(form, current_user)
        else:
            logger.error('Error in form validation')
            print('Error in form validation.')

    return render_template('index.html',
                           form=form,
                           video_form=video_form,
                           user=current_user)

@index_page.route('/logout')
def logout():
    session["user"] = None
    return redirect(url_for('index_page.home'))
