"""
You can set APP_CONFIG to point to this file to enable automatic reloading of
modules.
"""

reload = False
workers = 4
preload_app = True
threads = 4