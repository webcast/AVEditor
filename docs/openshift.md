# Openshift

> ⚠️ Secrets and service accounts are not restored. You need to create them manually. These include: 
> - Secret `gitlab-registry-auth`: To access the Gitlab Registry
> - Service Account `gitlabci-deployer`: To give Gitlab access to deploy in Openshift

## Add the Openshift deploy key to Gitlab

> To grant access to the private Gitlab repository

```
oc login openshift.cern.ch
oc project aveditor # 2 options
oc get secrets/sshdeploykey -o go-template='{{index .metadata.annotations "cern.ch/ssh-public-key"}}{{println}}'
```

## Deploy a Docker image 

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user registry-editor -z gitlabci-deployer
```

```bash
oc policy add-role-to-user view -z gitlabci-deployer
```

```bash
oc serviceaccounts get-token gitlabci-deployer
```

2. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<QA|MASTER|NEXT|TEST>`.
3. Generate a deploy token in Gitlab. This will generate a username and a password that will be added to Openshift later. 
   1. Needed rights for: read_registry
   2. The script is located on [/openshift/grant-registry-access.sh](/openshift/grant-registry-access.sh). Change the corresponging username/password.

- Imagestream being updated from Gitlab when `redeploy` stage finishes

To restore a backup of the elements in Openshift:

## Restore the config maps:
```bash
oc create -f openshift/configmap.yaml
```

Update the values in the configmap to match the correct ones.

The secrets can be generated using python:

```python
import os
os.urandom(20)
```

- By default, the ImageStream will take the `qa` Docker image, go to the ImageStream and change `qa` with `prod`.
- The route will point to https://localhost. Change this to the hostname of your application.

## Restore the webapp deployment

```bash
oc create -f openshift/webapp.yaml
```