ADMIN_EGROUP = "webcast-team"
APP_PORT = 8080
SECRET_KEY = "E\xf0\xd2G\xd5\x0bJ\xfd\x0b\xc7\xdc\x8c\xfb\xbd\xf7\xcd&C\xa3\xbc\xc8\xf7\xeb5"

# Debug and logging configuration
DEBUG = True
IS_LOCAL_INSTALLATION = True
TESTING = False

SEND_EMAIL = True
MAIL_HOSTNAME = ""
MAIL_FROM = ""
MAIL_TO = ""


#
# LOGS
#
LOG_LEVEL = "DEV"
LOG_REMOTE_ENABLED = True
LOG_REMOTE_URL = "http://monit-logs.cern.ch:10012"
LOG_REMOTE_TYPE = "aveditor-test"
LOG_REMOTE_PRODUCER = "webcast"

# Whether or not to use the wsgi Proxy Fix
USE_PROXY = True

API_URL_PREFIX = "/api"
ALLOWED_API_IPS = ["188.184.36.202", "188.184.36.203", "128.142.200.46", "137.138.108.121", "188.185.65.83"]
FETCH_UPCOMING_API_KEY = ""

# Oauth config
CERN_OAUTH_CLIENT_ID = ""
CERN_OAUTH_CLIENT_SECRET = ""
CERN_OAUTH_AUTHORIZE_URL = "https://oauth.web.cern.ch/OAuth/Authorize"
CERN_OAUTH_TOKEN_URL = "https://oauth.web.cern.ch/OAuth/Token"

# Stats Config
CONF_STATS_DIRPATH_XML_DATA = "/eos/user/m/micala/webcast_stats/test/data"
CONF_STATS_SUBMIT_URL = "https://test-webcast-stats.web.cern.ch/generate-stats"
STATS_API_KEY = ""

# DB Config
DB_NAME = "postgres"
DB_PASS = "postgres"
DB_PORT = 5432
DB_SERVICE = "postgres"
DB_USER = "postgres"

SQLALCHEMY_POOL_SIZE = 5
SQLALCHEMY_POOL_TIMEOUT = 10
SQLALCHEMY_POOL_RECYCLE = 120
SQLALCHEMY_MAX_OVERFLOW = 3

# Indico API
INDICO_URL = "https://indico.cern.ch"
INDICO_API_KEY = ""
INDICO_API_SECRET_KEY = ""

# WOWZA
WOWZA_ORIGIN_URL = "https://wowzaqaorigin.cern.ch"
WOWZA_EDGE_URL = "https://wowzaqaedge.cern.ch"
WOWZA_SMIL_FOLDER = "/eos/user/m/micala/www/test/smil"
WOWZA_USERNAME = "webcast"
WOWZA_PASSWORD = "webcast0"

# CDS
CDS_SERVER = "https://cdsweb.cern.ch/record/"
CDS_API_URL = "https://cdsweb.cern.ch/search?p=300__a%3A%22Streaming+video%22+and+collection%3AIndico+and+%28collection%3ATALK+or+collection%3ARestricted_ATLAS_Talks+or+collection%3ARestricted_CMS_Talks%29+AND+8567_x%3Apngthumbnail&f=&action_search=Search&c=CERN+Document+Server&rg=12&sc=0&of=xm&sf=269__c&so=d"

# Images
IMAGES_FOLDER = "images"
PERMANENT_STREAM_IMAGES_FOLDER = "permanent"
EVENTS_IMAGES_FOLDER = "events"

UPLOADED_FILES_DEST = "/eos/user/m/micala/www/test/uploads"
UPLOADED_IMAGES_DEST = "/eos/user/m/micala/www/test/uploads/images"
TEMP_FILES_DEST = "/opt/app-root"

STATIC_FILES_PATH = "/opt/app-root/app/static"
DEFAULT_IMAGES_FOLDER = "	 images/default"
DEFAULT_CATEGORIES_FOLDER = "categories"
DEFAULT_EVENTS_FOLDER = "events"

UPLOADED_EVENTS_IMAGES_DEST = "/eos/user/m/micala/www/test/uploads/events"


JOBS = [
    {
        'id': 'update_cds_records',
        'func': 'app.cli:update_cds_records_job',
        'args': (),
        'trigger': 'interval',
        'minutes': 15
    },
    {
        'id': 'update_upcoming_events',
        'func': 'app.cli:update_upcoming_events_job',
        'args': (),
        'trigger': 'interval',
        'minutes': 30
    }
]

SCHEDULER_API_ENABLED = True

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = ""
CACHE_REDIS_PORT = ""
CACHE_OAUTH_TIMEOUT = 300

#
# Analytics
#
ANALYTICS_GOOGLE_ID = "UA-1536056-6"
ANALYTICS_PIWIK_ID = "3722"

BUILD_ASSETS = False
